var xx,yy,c1,c2;

if (room = room_about) { exit; }

jump_height = land_jump_height;
grav_mod = land_grav_mod;
grav_check = land_grav_check;
    
if (inwater) 
{
    jump_height = water_jump_height;
    grav_mod = water_grav_mod;
    grav_check = water_grav_check;
}

// Apply gravity (and jumping)
y = y+grav;
grav+=grav_mod;
if( grav>=grav_check ) grav=grav_check;

// If falling, check UNDER the player
if( grav<0 )
{
    if( dir=1){
        sprite_index = spr_jump_right;
    }else{
        sprite_index = spr_jump_left;
    }
    c2 = -1;
    c1 = GetCollision(x,y);
    if( (x&$1f)>0 ) {
        c2=GetCollision(x+32,y);
    }
    if( c1>0 || c2>0 )
    {
        grav=0;
        y = (y&$ffffffe0)+32;
    }
}
else{
    // Otherwise, check above player
    if( jump )
    {
        if( dir=1){
            sprite_index = spr_fall_right;
        }else{
            sprite_index = spr_fall_left;
        }    
    }else{
        grav=0;
        jump=true;
    }
    
    
    c2 = -1;
    
    c1 = GetCollision(x,y+32);
    

    
    if( (x&$1f)>0 ) {
        c2=GetCollision(x+32,y+32);
    }
    
    if( c1>0 || c2>0 )
    {
  
        y = (y&$ffffffe0);
        jump=0;
        
        if( dir=1){
            sprite_index = spr_walk_right;
        }else{
            sprite_index = spr_walk_left;
        }           
    }
    
}    




// If moving left, check LEFT collision
if( keyboard_check(vk_left) ) 
{
    dir=-1;
    if(!jump){
        sprite_index = spr_walk_left;
    }
    x=x-xspeed;
    c2=-1;
    c1 = GetCollision(x,y);
    if( (y&$1f)>0 ) c2=GetCollision(x,y+32);
    if(  c1>0 ) || ( c2>0 )
    {
        x = (x&$ffffffe0)+32;
    }    
}else if( keyboard_check(vk_right) )
{
    // Otherwise, check collision to the right
    dir=1;
    if(!jump){
        sprite_index = spr_walk_right;
    }
    x=x+xspeed;
    c2 = -1;
    c1 = GetCollision(x+32,y);
    if( (y&$1f)>0 ) c2=GetCollision(x+32,y+32);
    if(  c1>0 ) || ( c2>0 )
    {
        x = (x&$ffffffe0);
    }    
} else {
    // If standing still, don't animate
    image_index = 0;
}





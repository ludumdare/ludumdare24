/// Reset the Game Variables

global.food = 0;
global.foodgoal = 1;
global.levelcompleted = false;
global.currentlevel = 1;
global.previouslevel = -1;
global.version = "0.4";

score = 0;
lives = 3;

